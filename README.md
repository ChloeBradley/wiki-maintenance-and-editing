A series of small scripts and other miscellaneous tools I use to throw [the wiki](https://barrelwisdom.com/hub) together, as well as notes-to-self to stay organized on this ever expanding project.


# Things I Ought To Remember

Process to create new wiki projects:

* Extract Mediawiki to the proper directory
* Follow the normal steps to create a new wiki (and use the proper database to avoid another oopsie)
* Generate the localsettings.php file
* Put the hidden passwordy parts into the hidden passwordy file. Disgusting plaintext passwords will not be in my web directory.
* Put the non-hidden harmless settings stuff in the shared localsettings file
* Delete the mediawiki stuff I generated
* Create symlink
* Edit .htaccess
* Edit and run wikiupdate.sh to update everything
* Edit and run interwiki.py to properly link between everything

# To Dos

Higher priority:

* Set up SEO for BR and Totori
* Flavor text for Totori items
* Evaluate Tweeki skin
* BR: Candy item location, Dark Cave testing, finish table conversions, add SEO, get item images, upload demon images
* Finish Shallie
* Start Sophie/Firis, thanks to the crazy people helping me
 
Lower Priority:

* NOA2: actually finish the game so I can torture myself with another project, get image assets
* Either learn Lua or figure out another way to speed up Semantic Mediawiki results (will try memcached setting first -- doesn't seem to work by default)
* Totori: enhance appearance, actually add in those missing earth effects (whoops) and renumber all effects
* E&L: Edit properties with equip data, update Ultimate Item properties, figure out a better compromise between desktop and mobile for giant tables
* Automate creation of new wikis via install.php?
 
# Project Status
 
Mature

* Totori
* E&L
 
In Progress

* Shallie: 50%
* Blue Reflection: 70%
 
Started

* NOA2: Data acquired; conversion necessary
* Sophie: Images acquired, lots of synthesis data, conversion necessary, high priority
* Firis: Crazy amounts of data to piece together and complete, high priority
 
Not Started

* Rorona -- Text dump acquired; Images exist online, looks to be next in the pipeline
* Meruru -- Text dump acquired; Images acquried
* Ayesha -- Text dump acquired; Images exist online
* AT3 -- Complete-ish data, low priority
* NOA1 -- I like to pretend this doesn't exist; mechanics data available, can probably acquire text dump, lowest priority
* Those Seven PS2 games: Maybe in a million years, non-priority
* Annie: Maybe for giggles one day, non-priority
* Marie + Elie: May be able to acquire text dump
* ANS: Just listing this because I listed everything else, non-priority
* L&S: Awaiting the end of March to start project, highest priority