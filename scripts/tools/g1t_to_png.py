# Converts g1t images to DDS, then to PNG.

import os
import glob

ls = os.listdir(os.getcwd())

for item in ls:
	print(item)
	if'.g1t' in item and '.dds' not in item and '.png' not in item:
		os.system("HyoutaTools.exe Gust.g1t.Extract " + item)
		
ls = os.listdir(os.getcwd())

for item in ls:
	print(item)
	if '.dds' in item and '.png' not in item:
		os.system("magick" + " " + item + " " + item[:-3] +"png")