#!/usr/bin/python

# An easy script to update interwiki tables whenever I create a new wiki. This allows me to link between my own websites.

import MySQLdb
import getpass

prefixes = [
        ["","totori"],
        ["","escha"],
        ["","shallie"],
        ["","sophie"],
        ["","firis"],
        ["","bluereflection"],
        ["","noa2"],
        ["","hub"] ]

new = ["",""]

usr = raw_input('User: ')
pw = getpass.getpass()
database = raw_input('db: ')
conn = MySQLdb.connect(host="localhost", user=str(usr), passwd=str(pw),db=str(database))
x = conn.cursor()
try:
	for ls in prefixes:
		x.execute("INSERT INTO " + new[0] + "_interwiki (iw_prefix, iw_url, iw_local, iw_trans) VALUES (\'" + ls[1]  + "\', \'https://barrelwisdom.com/" + ls[1]  + "/$1\', 1, 0);")
		x.execute("INSERT INTO " + ls[0]  + "_interwiki (iw_prefix, iw_url, iw_local, iw_trans) VALUES (\'" + new[1] + "\', \'https://barrelwisdom.com/" + new[1] + "/$1\', 1, 0);")
	conn.commit()
except (MySQLdb.Error, MySQLdb.Warning) as e:
	print(e)
	conn.rollback()
conn.close()

# It's annoying to set up the privacy policy/disclaimer/about pages so I automate them. I do this now because this content is on the hub.

os.system("php /var/www/html/w/maintenance/importTextFiles.php --overwrite --wiki /" + new[1] + "~/maintenance/footersettings/*.txt")