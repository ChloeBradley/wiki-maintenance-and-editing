#!/usr/bin/python

# One of these years I'll bother to use shell scripts rather than endlessly rely on python as a crutch

import os

ls = ['(A Clothes)', '(A Heavy Armor)', '(A Light Armor)', '(Accessory)', '(Adventure)', '(Ancient Power)', '(Animal)', '(Antique)', '(Apple)', '(Beehive)', '(Bomb)', '(Book)', '(Clay)', '(Cloth)', '(Core)', '(Dragon)', '(Elixir)', '(Feather)', '(Food)', '(Fruit)', '(Fuel)', '(Gas)', '(Grain)', '(Gunpowder)', '(Jewel)', '(Liquid)', '(Lumber)', '(Magic Grass)', '(Magic Tool)', '(Meat)', '(Medic. Ing.)', '(Medicine)', '(Metal)', '(Mystery)', '(Natural)', '(Oil)', '(Ore)', '(Paper)', '(Plant)', '(Seasoning)', '(Skin)', '(Slag Parts)', '(Smell)', '(Smelly)', '(Snack)', '(Spark)', '(String)', '(Sundry)', '(Supplement)', '(Synthesis)', '(Vegetable)', '(W Accessory)', '(W Alch. Sword)', '(W Bag)', '(W Broomstick)', '(W Club)', '(W Cookware)', '(W Gun)', '(W Hammer)', '(W Longsword)', '(W Rapier)', '(W Staff)']

for item in ls:
        os.system("php /var/www/html/w/maintenance/getText.php \"" + item + "\" --wiki /escha | sed -e 's/name==/name=/' | php /var/www/html/w/maintenance/edit.php \"" + item + "\" --wiki /escha")
