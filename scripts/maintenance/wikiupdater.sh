#!/bin/bash

php /var/www/html/w/maintenance/update.php --skip-external-dependencies --quick --wiki /hub
php /var/www/html/w/maintenance/update.php --skip-external-dependencies --quick --wiki /totori
php /var/www/html/w/maintenance/update.php --skip-external-dependencies --quick --wiki /escha
php /var/www/html/w/maintenance/update.php --skip-external-dependencies --quick --wiki /shallie
php /var/www/html/w/maintenance/update.php --skip-external-dependencies --quick --wiki /sophie
php /var/www/html/w/maintenance/update.php --skip-external-dependencies --quick --wiki /firis
php /var/www/html/w/maintenance/update.php --skip-external-dependencies --quick --wiki /bluereflection
php /var/www/html/w/maintenance/update.php --skip-external-dependencies --quick --wiki /noa2