Data generated from: https://docs.google.com/spreadsheets/d/1P2AjlL-9C0nXv_yTQzbB0n-SdYTmgyfEkfoZixIwifk/edit#gid=0

When multiple text files are generated:

```php importTextFiles.php -u "LoserBot" --wiki bluereflection -s "Summary" --overwrite ~/directory/*.txt```

https://www.mediawiki.org/wiki/Manual:ImportTextFiles.php