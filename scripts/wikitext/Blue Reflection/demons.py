# Generates text files for each demon. These text files are necessary for mass upload to wiki.

num = 0
with open('Demons.tsv') as f:
	for line in f:
		if line:
			num += 1
			arr = line.split('\t')
			filename = arr[0]
			text = "{{Demon\n"
			text += "|number=" + str(num) + "\n"
			text += "|type=" + arr[1] + "\n"
			text += "|Slash=" + arr[8] + "\n"
			text += "|Impact=" + arr[9] + "\n"
			text += "|Pierce=" + arr[10] + "\n"
			text += "|Heart=" + arr[11] + "\n"
			text += "|HP=" + arr[2] + "\n"
			text += "|ATK=" + arr[3] + "\n"
			text += "|DEF=" + arr[4] + "\n"
			text += "|SPD=" + arr[5] + "\n"
			text += "|LUK=" + arr[6] + "\n"
			text += "|location=" + arr[7] + "\n"
			text += "|flavortext=" + arr[12].rstrip() + "\n"
			text += "}}\n"
			
			newfile = open(filename + ".txt", "w")
			newfile.write(text)
			newfile.close()