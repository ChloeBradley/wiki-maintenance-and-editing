# Generates text files for each item. These text files are necessary for mass upload to wiki.

num = 0
with open('Items.tsv') as f:
	for line in f:
		if line:
			num += 1
			arr = line.split('\t')
			filename = arr[0]
			text = "{{Item\n"
			text += "|number=" + str(num) + "\n"
			text += "|type=" + arr[2] + "\n"
			text += "|location=" + arr[4] + "\n"
			text += "|dropped=" + arr[6] + "\n"
			text += "|effect=" + arr[3] + "\n"
			text += "|acquisition=" + arr[5] + "\n"
			text += "|missions=" + arr[8].rstrip() + "\n"
			text += "|materials=" + arr[7] + "\n"
			text += "|flavortext=" + arr[1] + "\n"
			text += "}}\n"
			
			newfile = open(filename + ".txt", "w")
			newfile.write(text)
			newfile.close()